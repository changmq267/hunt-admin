package com.hunt.jms.demo;

import org.apache.activemq.command.ActiveMQTextMessage;
import org.springframework.jms.listener.adapter.MessageListenerAdapter;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.Session;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * @Author ouyangan
 * @Date 2017/1/17/16:11
 * @Description
 */
public class SubDemoB implements MessageListener {
    @Override
    public void onMessage(Message message) {
        try {
            System.out.println();
            Thread.currentThread().sleep(2000L);
            ActiveMQTextMessage message1 = (ActiveMQTextMessage) message;
            System.out.println("B接收消息-> " +message1.getText());
        } catch (InterruptedException | JMSException e) {
            e.printStackTrace();
        }
    }
}
