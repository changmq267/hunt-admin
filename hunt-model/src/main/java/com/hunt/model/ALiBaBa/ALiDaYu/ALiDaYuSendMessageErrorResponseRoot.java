package com.hunt.model.ALiBaBa.ALiDaYu;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * @Author ouyangan
 * @Date 2017/1/16/16:08
 * @Description
 */
public class ALiDaYuSendMessageErrorResponseRoot {
    private ALiDaYuSendMessageErrorResponse error_response;

    public ALiDaYuSendMessageErrorResponse getError_response() {
        return error_response;
    }

    public void setError_response(ALiDaYuSendMessageErrorResponse error_response) {
        this.error_response = error_response;
    }

    @Override
    public String toString() {
        return "ALiDaYuSendMessageErrorResponseRoot{" +
                "error_response=" + error_response +
                '}';
    }
}
